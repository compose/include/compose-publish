;; Do not prompt for code block evaluation.
(setq org-confirm-babel-evaluate nil)

(defun org-babel-execute-file (plist filename pub-dir)
  ;; A custom publishing function for executing all Babel source code blocks in
  ;; the Org file `filename'. Note that the function does not actually publish
  ;; anything to `pub-dir'.
  (progn
    (find-file filename)
    (org-babel-execute-buffer)
    (set-buffer-modified-p nil)
    (kill-buffer)))

(defun execute-block-in-file (block file)
  ;; Executes the named source code `block' in `file' without altering the
  ;; latter.
  (progn
    (find-file file)
    (org-babel-goto-named-src-block block)
    (org-babel-execute-src-block)
    (set-buffer-modified-p nil)
    (kill-buffer)))

(defun disable-babel (&optional project-plist)
  ;; Disable Babel code evaluation.
  (setq org-export-babel-evaluate nil))

(defun enable-babel (&optional project-plist)
  ;; Enable Babel code evaluation.
  (setq org-export-babel-evaluate t))
