(require 'org)
(require 'ox-publish)
(require 'htmlize)
(require 'org-ref)
;; (require 'org-ref-helm-bibtex)
(require 'oc)
(require 'oc-biblatex) ;; for latex/pdf
(require 'citeproc) ;; for html
(require 'oc-csl) ;; for html
(require 'ox-beamer)
(require 'ox-re-reveal)
(require 'ox-extra)
(require 'org-id)

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process (list "latexmk --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("compas2021"
                 "\\documentclass{compas2021}
\\usepackage{hyperref}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("IEEEtran"
                 "\\documentclass{IEEEtran}
\\usepackage{hyperref}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("llncs"
                 "\\documentclass{llncs}
\\usepackage{hyperref, wrapfig}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("siamart220329"
                 "\\documentclass{siamart220329}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("acmart"
                 "\\documentclass{acmart}
[NO-DEFAULT-PACKAGES]
[EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(defun remove-from-org-latex-default-packages-alist (package)
  "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-default-packages-alist'. This
is useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
  (let ((output '()))
    (dolist (pkg org-latex-default-packages-alist)
      (unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
    (setq org-latex-default-packages-alist output)))

(defun remove-from-org-latex-packages-alist (package)
  "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-packages-alist'. This is
useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
  (let ((output '()))
    (dolist (pkg org-latex-packages-alist)
      (unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
    (setq org-latex-packages-alist output)))

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((org . t)
   (latex . t)
   (C . t)
   (R . t)
   (python . t)
   (shell . t)))

(setq org-confirm-babel-evaluate nil)

(add-to-list 'org-src-lang-modes '("c++" . c++))

(setq org-babel-python-command "python3")

;; Do not export org
;; (setq org-export-with-todo-keywords nil)

;; Ignore headlines with :ignore: tag:
(ox-extras-activate '(ignore-headlines))

;; Define a function for selective content export based on the backend.
(defun filter-export (backend)
  (if (org-export-derived-backend-p backend 'latex)
      (org-map-entries '(org-toggle-tag "noexport" "on") "+htmlonly"))
  (if (org-export-derived-backend-p backend 'html)
      (org-map-entries '(org-toggle-tag "noexport" "on") "+texonly")))
(add-hook 'org-export-before-parsing-hook 'filter-export)


;; Refinement for the two main available citation management tools: org-cite
;; (oc.el) and org-ref

;; org-cite (oc.el): we choose the biblatex export processor for latex (and
;; derived back-ends) documents and csl otherwise (in particular for html
;; export)
(setq org-cite-export-processors
      '((latex biblatex)
        (t csl)))

;; org-ref: add an entry format to org-ref for @misc bibtex entries; the other
;; values are left unchanged.
(setq org-ref-bibliography-entry-format
      '(("article" . "%a, %t, <i>%j</i>, <b>%v(%n)</b>, %p (%y). <a href=\"%U\">link</a>. <a href=\"http://dx.doi.org/%D\">doi</a>.")
        ("book" . "%a, %t, %u (%y).")
        ("techreport" . "%a, %t, %i, %u (%y).")
        ("proceedings" . "%e, %t in %S, %u (%y).")
        ("inproceedings" . "%a, %t, %p, in %b, edited by %e, %u (%y).")
        ("misc" . "%a, %t (%y).")))

;; Default packages
;; Note that: amsmath and amssymv are already included by default in
;; org-latex-default-packages-alist
(add-to-list 'org-latex-packages-alist '("" "braket"))
(add-to-list 'org-latex-packages-alist '("" "amsopn"))
(add-to-list 'org-latex-packages-alist '("algo2e,ruled,vlined" "algorithm2e"))
(add-to-list 'org-latex-packages-alist '("" "tikz"))
(add-to-list 'org-latex-packages-alist '("" "svg"))

;; Handling latex macros
(add-to-list 'org-src-lang-modes '("latex-macros" . latex))

(setq org-babel-default-header-args:latex-macros
      '((:results . "raw")
        (:exports . "results")
        (:tangle . "latex-macros.tex")))

(defun prefix-all-lines (pre body)
  (with-temp-buffer
    (insert body)
    (string-insert-rectangle (point-min) (point-max) pre)
    (buffer-string)))

(add-hook 'org-export-before-processing-hook #'org-babel-tangle)

(defun org-babel-execute:latex-macros (body _params)
  (concat
   (prefix-all-lines "#+LATEX_HEADER: " body)
   "\n#+HTML_HEAD_EXTRA: <div style=\"display: none\"> \\(\n"
   (prefix-all-lines "#+HTML_HEAD_EXTRA: " body)
   "\n#+HTML_HEAD_EXTRA: \\)</div>\n"))

;; To include easy color links in org-mode
;; now simply use [[color:red][text to be in red]]
;; https://orgmode.org/worg/org-faq.html#org64ae339
(org-add-link-type
 "color"
 (lambda (path)
   (message (concat "color "
                    (progn (add-text-properties
                            0 (length path)
                            (list 'face `((t (:foreground ,path))))
                            path) path))))
 (lambda (path desc format)
   (cond
    ((eq format 'html)
     (format "<span style=\"color:%s;\">%s</span>" path desc))
    ((eq format 'latex)
     (format "{\\color{%s}%s}" path desc)))))

;; fullpath from relative path
;; http://xahlee.info/emacs/emacs/elisp_relative_path.html
(defun compose-get-fullpath (@file-relative-path)
  "Return the full path of *file-relative-path, relative to caller's file location.

Example: If you have this line
 (xah-get-fullpath \"../xyz.el\")
in the file at
 /home/joe/emacs/emacs_lib.el
then the return value is
 /home/joe/xyz.el
Regardless how or where emacs_lib.el is called.

This function solves 2 problems.

① If you have file A, that calls the `load' on a file at B, and B calls `load' on file C using a relative path, then Emacs will complain about unable to find C. Because, emacs does not switch current directory with `load'.

To solve this problem, when your code only knows the relative path of another file C, you can use the variable `load-file-name' to get the current file's full path, then use that with the relative path to get a full path of the file you are interested.

② To know the current file's full path, emacs has 2 ways: `load-file-name' and `buffer-file-name'. If the file is loaded by `load', then `load-file-name' works but `buffer-file-name' doesn't. If the file is called by `eval-buffer', then `load-file-name' is nil. You want to be able to get the current file's full path regardless the file is run by `load' or interactively by `eval-buffer'."

  (concat (file-name-directory (or load-file-name buffer-file-name)) @file-relative-path)
  )


;; ob-latexpicture
(org-babel-load-file (compose-get-fullpath "./ob-latexpicture/ob-latexpicture.org"))

;; Refine rendering of pdf source code through =minted=
(add-to-list
 'org-latex-packages-alist '("" "minted"))
(add-to-list
 'org-latex-packages-alist '("" "color"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options '(("breaklines" "true")
                                 ("breakanywhere" "true")))

;; TODO Refine the html rendering of source code in batch mode
;; Read: https://emacs.stackexchange.com/questions/31439/how-to-get-colored-syntax-highlighting-of-code-blocks-in-asynchronous-org-mode-e
;; In our workflow, we enable css
(setq org-html-htmlize-output-type 'css)
;; and rely on https://mfelsoci.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/css/htmlize.css
;; In more complicated scenarios it might be required to generate an ad hoc css via (org-html-htmlize-generate-css)
;; Conversely, in an interactive scenario one might want to fall back to: (setq org-html-htmlize-output-type 'inlince-css)

;; Allow to emphasize letters in the middle of a word (for the Panic acronym)
(setcar org-emphasis-regexp-components " \t('\"{[:alpha:]")
(setcar (nthcdr 1 org-emphasis-regexp-components) "[:alpha:]- \t.,:!?;'\")}\\")
(org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)

;; Custom publishing function for LaTeX files. This is useful when we need to
;; publish a manually written LaTeX file to PDF (e. g. a poster).
(defun latex-publish-to-pdf (plist filename pub-dir)
  (let
      ;; Keep the initial directory.
      ((cwd default-directory))
    ;; Navigate to the directory of the LaTeX file to publish.
    (cd (file-name-directory filename))
    ;; Call the interal publishing function.
    (org-latex-compile filename)
    ;; Publish the PDF file to the destination set in the project's alist.
    (org-publish-attachment
     plist
     (concat (file-name-sans-extension filename) ".pdf")
     pub-dir)
    ;; Return to the initial directory.
    (cd cwd)))

;; Verbose errors for pdf generation
;; https://www.pank.eu/blog/blog-setup.html
;; Note: we use /dev/tty instead of /dev/stdout: https://stackoverflow.com/questions/16747449/runuser-permission-denied-on-dev-stdout
;; Note: 2022/12/20: We shall even use "$(tty)" for more portability (thanks Antoine Jego)
(defun org-latex-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case err
      (org-latex-publish-to-pdf
       plist filename pub-dir)
    ;; Handle Org errors at first.
    (user-error
     (message "%s" (error-message-string err)))
    ;; If the Org source is fine, handle LaTeX errors.
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (message (buffer-string))))))

(defun org-beamer-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case err
      (org-beamer-publish-to-pdf
       plist filename pub-dir)
    ;; Handle Org errors at first.
    (user-error
     (message "%s" (error-message-string err)))
    ;; If the Org source is fine, handle LaTeX errors.
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (message (buffer-string))))))

(defun org-babel-execute-file (plist filename pub-dir)
  ;; A custom publishing function for executing all Babel source code blocks in
  ;; the Org file `filename'. Note that the function does not actually publish
  ;; anything to `pub-dir'.
  (progn
    (find-file filename)
    (org-babel-execute-buffer)
    (set-buffer-modified-p nil)
    (kill-buffer)))

(defun execute-block-in-file (block file)
  ;; Executes the named source code `block' in `file' without altering the
  ;; latter.
  (progn
    (find-file file)
    (org-babel-goto-named-src-block block)
    (org-babel-execute-src-block)
    (set-buffer-modified-p nil)
    (kill-buffer)))

(defun disable-babel (&optional project-plist)
  ;; Disable Babel code evaluation.
  (setq org-export-babel-evaluate nil))

(defun enable-babel (&optional project-plist)
  ;; Enable Babel code evaluation.
  (setq org-export-babel-evaluate t))

;; Use timestamp to avoid publishing
(setq org-publish-use-timestamps-flag t)

;; This fix is necessary to obtain relative path when tangling with comments: link
;; It has been fix for org version 9.3.7 (but it's too)
(if (string< (org-version) "9.3.7")
    (load-file "./ob-tangle-fix.el") ())

;; width and height of reveal presentations
(setq org-re-reveal-width 1200)
(setq org-re-reveal-height 1000)

;; attachment for org-publish-attachment, can be used e.g; with:
;; :base-extension site-attachments
(defvar site-attachments
  (regexp-opt '("m" "py" "ipynb" "scm"
                "jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

;; This is the default org-publish-project-alist
;; Do not hesitate to override it
(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :exclude (regexp-opt '("draft" "include" "rawlatex" "public" "sitemap" "slides" "static"))
             :publishing-function '(org-latex-publish-to-pdf-verbose-on-error org-html-publish-to-html)
             :publishing-directory "./public"
             :auto-sitemap t
             :sitemap-filename "./sitemap-org.inc"
             :makeindex t
             :author "compose development team"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
       (list "site-org-attach"
             :base-directory "."
             :base-extension "py\\|ipynb\\|css\\|cpp\\|h\\|png\\|csv\\|txt\\|svg\\|yml\\|el\\|setup"
             :recursive t
             :exclude (regexp-opt '("draft" "include" "rawlatex" "public" "sitemap" "static"))
             :publishing-function '(org-publish-attachment)
             :publishing-directory "./public/"
             :auto-sitemap t
             :sitemap-filename "./sitemap-org-attach.inc")
       (list "site-static"
             :base-directory "./static"
             :base-extension "png\\|csv\\|txt\\|pdf\\|svg\\|yml\\|el\\|setup"
             :recursive t
             :publishing-function '(org-publish-attachment)
             :publishing-directory "./public/static/")
       (list "slides"
             :base-directory "./slides"
             :base-extension "org"
             :recursive t
             :exclude (regexp-opt '("draft" "include"))
             :publishing-function '(org-beamer-publish-to-pdf-verbose-on-error org-re-reveal-publish-to-reveal org-re-reveal-publish-to-reveal-client)
             :publishing-directory "./public/slides"
             :auto-sitemap t
             :sitemap-filename "../sitemap-org-slides.inc")
       (list "site-sitemap"
             :base-directory "sitemap"
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/sitemap"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
       (list "site" :components '("site-org" "site-org-attach" "site-static" "slides" "site-sitemap"))))


(provide 'compose-publish)
