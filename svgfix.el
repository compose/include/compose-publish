(defun fix-svg (&optional project-plist)
  ;; If used, these functions shall be executed after exporting a particular
  ;; item of the project alist.
  (let
      ((images
        (directory-files
         (concat
          (plist-get project-plist :publishing-directory) "/figures/results")
         t "svg$")))
    (dolist (image images)
      (message "Applying 'svgfix' on `%s'..." image)
      (shell-command
       (concat "svgfix " image)))))
