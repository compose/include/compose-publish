;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process
      (list
       "latexmk --shell-escape -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"))

;; Preserve user-defined labels during the export to PDF via LaTeX.
(setq org-latex-prefer-user-labels t)

(defun remove-from-org-latex-default-packages-alist (package)
  "Removes `package' from the list of packages that are included
by default when exporting from Org to LaTeX, i.e. from
`org-latex-default-packages-alist'. This is useful for controlling the order in
which some of the packages are loaded in order to prevent conflicts or other
undesirable behavior."
  (let ((output '()))
    (dolist (pkg org-latex-default-packages-alist)
      (unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
    (setq org-latex-default-packages-alist output)))

(defun remove-from-org-latex-packages-alist (package)
  "Removes `package' from the list of packages that are included by default when
exporting from Org to LaTeX, i.e. from `org-latex-packages-alist'. This is
useful for controlling the order in which some of the packages are loaded in
order to prevent conflicts or other undesirable behavior."
  (let ((output '()))
    (dolist (pkg org-latex-packages-alist)
      (unless (string= package (nth 1 pkg))
          (add-to-list 'output pkg t)))
    (setq org-latex-packages-alist output)))

;; Custom publishing function for LaTeX files. This is useful when we need to
;; publish a manually written LaTeX file to PDF (e. g. a poster).
(defun latex-publish-to-pdf (plist filename pub-dir)
  (let
      ;; Keep the initial directory.
      ((cwd default-directory))
    ;; Navigate to the directory of the LaTeX file to publish.
    (cd (file-name-directory filename))
    ;; Call the interal publishing function.
    (org-latex-compile filename)
    ;; Publish the PDF file to the destination set in the project's alist.
    (org-publish-attachment
     plist
     (concat (file-name-sans-extension filename) ".pdf")
     pub-dir)
    ;; Return to the initial directory.
    (cd cwd)))

;; Verbose errors for PDF publishing through LaTeX
(defun org-latex-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case err
      (org-latex-publish-to-pdf
       plist filename pub-dir)
    ;; Handle Org errors at first.
    (user-error
     (message "%s" (error-message-string err)))
    ;; If the Org source is fine, handle LaTeX errors.
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (message (buffer-string))))))

(defun org-beamer-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case err
      (org-beamer-publish-to-pdf
       plist filename pub-dir)
    ;; Handle Org errors at first.
    (user-error
     (message "%s" (error-message-string err)))
    ;; If the Org source is fine, handle LaTeX errors.
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (message (buffer-string))))))
